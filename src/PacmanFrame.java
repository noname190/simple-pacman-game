import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * JFrame Class for the pacman game
 */
public class PacmanFrame extends JFrame {

	public static final int FRAME_WIDTH = 500;
	public static final int FRAME_HEIGHT = 500;

	JPanel panel;

	/*
	 * Constructor that sets the dimension of JFrame and creates a panel
	 */
	public PacmanFrame() {
		setSize(FRAME_WIDTH, FRAME_HEIGHT);

		createPanel();
	}

	/*
	 * Creates the penal and adds it to the JFrame
	 */
	public void createPanel() {

		panel = new GamePanel();
		
		add(panel);//add panel to this frame

	}

}
