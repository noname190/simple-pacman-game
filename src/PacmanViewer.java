import java.awt.Dimension;

import javax.swing.JFrame;

/*
 * A Viewer class that instatiates the Frame and makes 
 * it close when the close button is pressed
 */
public class PacmanViewer {
	
	/*
	 * Main method for the whole program
	 * @param args Unused Argument
	 */
	public static void main(String[] args) {
		JFrame frame = new PacmanFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
	    frame.setMinimumSize(new Dimension(PacmanFrame.FRAME_WIDTH, PacmanFrame.FRAME_HEIGHT+20));
	}
}
