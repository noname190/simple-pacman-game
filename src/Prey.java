import java.awt.Shape;
import java.awt.geom.Ellipse2D;

/*
 * Prey Super class
 */
public class Prey extends Creature {
	
	protected Ellipse2D.Double fastPrey;
	/*
	 * Prey constructer that passes values to super
	 * @param x the x value
	 * @param y the y value of shape
	 * @param w the width of shape
	 * @param h the height of shape
	 */
	public Prey(int x, int y, int w, int h) {
		super(x,y,w,h);
	}

	/*
	 * Returns the shape (ellipse2d)
	 * @return the ellipse2d shape
	 */
	public Shape getShape(){
		return fastPrey;
	}
	
	
}
