import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.util.Random;
/*
 * FastPrey class that contains movement methods and the shapes to draw
 */
public class FastPrey extends Prey {
	Random random;
	int maxX;
	int minX;
	int maxY;
	int minY;
	double oldxV;
	double oldyV;
	int oldMaxX;
	int oldMinX;
	boolean evadeIsRunning;

	/*
	 * Constructer that creates the first prey ellipse and 
	 * initializes variables
	 * @param x the x location of ellipse
	 * @param y the y location of ellipse
	 * @param w the width of ellipse
	 * @param h the height of ellipse
	 */
	public FastPrey(int x, int y, int w, int h) {
		super(x, y, w, h);
		fastPrey = new Ellipse2D.Double(x, y, w, h);
		random = new Random();
		
		//Chose random velocitys from 1 to 4
		xVelocity = random.nextInt(4 - 1) + 1;
		yVelocity = random.nextInt(4 - 1) + 1;
		
		//Minx and maxX contain the max and min values that each prey can travel 
		//to in their designated pattern. Make the area that prey moves a third
		//of total frame width
		minX = (int) Math.abs(PacmanFrame.FRAME_WIDTH / 3 - fastPrey.getX());
		maxX = (int) Math.abs(PacmanFrame.FRAME_WIDTH / 3 + fastPrey.getX());
		minY = (int) Math.abs(PacmanFrame.FRAME_HEIGHT / 3 - fastPrey.getY());
		maxY = (int) Math.abs(PacmanFrame.FRAME_HEIGHT / 3 + fastPrey.getY());
		randomVelocity();
	}

	/*
	 * Randomly decides if pattern should be horrizontal or vertical
	 */
	private void randomVelocity() {
		//(Math.random() < 0.5)  is Method for random boolean
		if ((Math.random() < 0.5) == true) {
			yVelocity = 0;
		} else {
			xVelocity = 0;
		}
	}

	/*
	 * Draws the shapes to the panel
	 * @param g2 the Graphics2D variable to draw onto
	 */
	public void draw(Graphics2D g2) {
        g2.setColor(Color.cyan);
        g2.fillOval(getX(), getY(), getWidth(), getHeight()); //Draw Face
        g2.setColor(Color.black);
        //Not magic numbers. Instead there to fine tune the location of 
        //face elements
        g2.fillOval(getX() + 7, getY() + 8, 5, 5); // left eye
        g2.fillOval(getX() + 18, getY() + 8, 5, 5); // right eye
        g2.drawArc(getX()+5, getY()+8, 20,15, 180, 180); // mouth

	}
	/*
	 * Tells the fast prey how to move and makes sure it can't
	 * leave the frame dimensions
	 */
	public void move() {

		//Basic format is if direction is ...(up, down, left, right)... and not 
		//greater then frame then update
		//if creater then frame then reverse direcion
		
		if (xVelocity > 0) {
			if (fastPrey.getX() < maxX && fastPrey.getX() + xVelocity + fastPrey.getWidth() < PacmanFrame.FRAME_WIDTH) {

				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			} else if (fastPrey.getX() >= maxX
					|| fastPrey.getX()+fastPrey.getWidth()+xVelocity >= PacmanFrame.FRAME_WIDTH) {
				xVelocity = -xVelocity;
				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			}

		} else if (xVelocity < 0) {
			if (fastPrey.getX() > minX) {

				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			} else if (fastPrey.getX() <= minX) {
				xVelocity = -xVelocity;
				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			}
		} else if (yVelocity > 0) {
			if (fastPrey.getY() < maxY
					&& !(fastPrey.getY() +fastPrey.getWidth() + yVelocity >= PacmanFrame.FRAME_HEIGHT)) {

				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			} else if (fastPrey.getY() >= maxY
					|| fastPrey.getY() +fastPrey.getWidth() + yVelocity >= PacmanFrame.FRAME_HEIGHT) {
				yVelocity = -yVelocity;
				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			}

		} else if (yVelocity < 0) {
			if (fastPrey.getY() > minY) {

				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			} else if (fastPrey.getY() <= minY && fastPrey.getY() < maxY) {
				yVelocity = -yVelocity;
				fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
						fastPrey.getY() + yVelocity, width, height);
			}
		}
	}

	/*
	 * Returns the boudnding box of the fast prey
	 * @return a rectangle with the dimensions of the prey shape
	 */
	public Rectangle getBoudingBox() {
		return new Rectangle((int) fastPrey.getX(), (int) fastPrey.getY(),
				(int) fastPrey.getWidth(), (int) fastPrey.getHeight());
	}

	/*
	 * Returns the x value of the predator shape
	 * return the x value of predator
	 */
	public int getX() {
		return (int) fastPrey.getX();
	}

	/*
	 * Returns the y value of the prey shape
	 * return the y value of prey
	 */
	public int getY() {
		return (int) fastPrey.getY();
	}

	/*
	 * Returns the Width value of the prey shape
	 * return the Width value of prey
	 */
	public int getWidth() {
		return (int) fastPrey.getWidth();
	}
	/*
	 * Returns the Height value of the prey shape
	 * return the Height value of prey
	 */
	public int getHeight() {
		return (int) fastPrey.getHeight();
	}

	/*
	 * Method that saves the old direction and velocity
	 * of the prey before it evades
	 */
	public void evadeStart() {
		oldxV = xVelocity;
		oldyV = yVelocity;
		oldMaxX = maxX;
		oldMinX = minX;
		evadeIsRunning = true;
	}

	/*
	 * Restores the old variables of the maxX minX and the velocity
	 */
	public void evadeStop() {
		xVelocity = oldxV;
		yVelocity = oldyV;
		minX = oldMinX;
		maxX = oldMaxX;
		evadeIsRunning = false;
	}

	/*
	 * Checks if prey is evading
	 * @return boolean if evade is running
	 */
	public boolean evadeisRunning() {
		return evadeIsRunning;
	}

	/*
	 * Tells the prey to evade
	 * @param x the x location of the predator
	 * @param y the y location of the predator
	 */
	public void evade(int x, int y) {

		//Evades with random speeds.
		//Contains info on what to do if predator is in center and 
		//prey is either top left, top right, bottom left, bottom right
		if(x < fastPrey.getX() && y > fastPrey.getY()){
			xVelocity = 5;
			yVelocity = 3;
		} else if (x > fastPrey.getX() && y > fastPrey.getY()){
			xVelocity = -3;
			yVelocity = 4;
		}else if (x < fastPrey.getX() && y < fastPrey.getY()){
			xVelocity = 2;
			yVelocity = -4;
		} else if (x > fastPrey.getX() && y < fastPrey.getY() ){
			xVelocity = -3;
			yVelocity = -3;
		}
		minX = 0;
		maxX = 0;
		
		//Make sure the new velocity won't make the prey be longer then frame height
		if (!(xVelocity + fastPrey.getX() + fastPrey.getWidth() > PacmanFrame.FRAME_WIDTH
				|| yVelocity + fastPrey.getY() > PacmanFrame.FRAME_HEIGHT
				|| xVelocity + fastPrey.getX() <= 0 || yVelocity
				+ fastPrey.getY() <= 0)) {
			fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
					fastPrey.getY() + yVelocity, width, height);
		}
		evadeStop();
	}
}
