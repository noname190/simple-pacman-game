import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.util.Random;

public class SlowPrey extends Prey {

	Random random;
	int maxX;
	int minX;
	int maxY;
	int minY;
	int count;

	/*
	 * Constructer that creates the first prey ellipse and initializes variables
	 * 
	 * @param x the x location of ellipse
	 * 
	 * @param y the y location of ellipse
	 * 
	 * @param w the width of ellipse
	 * 
	 * @param h the height of ellipse
	 */
	public SlowPrey(int x, int y, int w, int h) {
		super(x, y, w, h);
		fastPrey = new Ellipse2D.Double(x, y, w, h);
		random = new Random();
		xVelocity = 0.05 + (0.09 - 0.07) * random.nextDouble();//random number form .05 to .09
		yVelocity = random.nextDouble();
		minX = (int) Math.abs(PacmanFrame.FRAME_WIDTH / 4 - fastPrey.getX());
		maxX = (int) Math.abs(PacmanFrame.FRAME_WIDTH / 4 + fastPrey.getX());
		minY = (int) Math.abs(PacmanFrame.FRAME_HEIGHT / 4 - fastPrey.getY());
		maxY = (int) Math.abs(PacmanFrame.FRAME_HEIGHT / 4 + fastPrey.getY());
		randomVelocity();
	}

	/*
	 * Determines the pattern of the prey
	 */
	private void randomVelocity() {
		//Method works as a boolean random (Math.random() < 0.5)
		if ((Math.random() < 0.5) == true) {
			yVelocity = 0;
		} else {
			xVelocity = 0;
		}

	}

	/*
	 * Draws the shapes to the panel
	 * 
	 * @param g2 the Graphics2D variable to draw onto
	 */
	public void draw(Graphics2D g2) {
		g2.setColor(Color.white);
		g2.fill(fastPrey);
		g2.setColor(Color.black);
		g2.fill(new Ellipse2D.Double(getX() + 3, getY() + 3, getWidth() / 2,
				getHeight() / 2));
	}

	/*
	 * Tells the prey to move and determines where it should move
	 */
	public void move() {
		// Keep the movement close to the original point
		if (count / 10 == 1) {
			count = 0;

			int number = random.nextInt(5);
			//random cases that make the litle prey rotate around original area;
			switch (number) {
			case 0:
				break;
			case 1:
				xVelocity = -xVelocity;
				break;
			case 2:
				yVelocity = -yVelocity;
				break;
			case 5:
				xVelocity = -xVelocity;
				yVelocity = -yVelocity;
				break;
			}
		}

		if (fastPrey.getX() + fastPrey.getWidth() + xVelocity > PacmanFrame.FRAME_WIDTH) {
			xVelocity = -xVelocity;
		} else if (fastPrey.getX() < 0 - fastPrey.getWidth()) {
			xVelocity = -xVelocity;
		} else if (fastPrey.getY() < 0 - fastPrey.getWidth()) {
			yVelocity = -yVelocity;
		} else if (fastPrey.getY() + fastPrey.getWidth() + yVelocity > PacmanFrame.FRAME_HEIGHT) {
			yVelocity = -yVelocity;
		}
		count++;
		if (fastPrey.getY() + yVelocity > fastPrey.getWidth()) {
			fastPrey = new Ellipse2D.Double(fastPrey.getX() + xVelocity,
					fastPrey.getY() + yVelocity, width, height);
		}
	}

	/*
	 * Returns the bouding box of the prey
	 * 
	 * @return a Rectangle with the bouding variables
	 */
	public Rectangle getBoudingBox() {
		return new Rectangle((int) fastPrey.getX(), (int) fastPrey.getY(),
				(int) fastPrey.getWidth(), (int) fastPrey.getHeight());
	}

	/*
	 * Returns the x location of the prey shape
	 * 
	 * @return the x location of the prey
	 */
	public int getX() {
		return (int) fastPrey.getX();
	}

	/*
	 * Returns the y location of the prey shape
	 * 
	 * @return the y location of the prey
	 */
	public int getY() {
		return (int) fastPrey.getY();
	}

	/*
	 * Returns the width of the prey shape
	 * 
	 * @return the width of the prey
	 */
	public int getWidth() {
		return (int) fastPrey.getWidth();
	}

	/*
	 * Returns the height of the prey shape
	 * 
	 * @return the height of the prey
	 */
	public int getHeight() {
		return (int) fastPrey.getHeight();
	}
}