import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;

/*
 * A JPanel that draws the shapes and contains the game logic and 
 * listeners and the time
 */
public class GamePanel extends JPanel {
	private static final int DELAY = 10;
	private static final long SECONDS_IN_MINUTE = 60;
	private Predator predator;
	ArrayList<Creature> prey = new ArrayList<Creature>();// Arraylist for prey
	final Timer timer;
	long startTime;// time when game started
	long endTime;// time when game ended
	MouseListener mListener;
	int count = 0;// counter to test if the actionperformed method has run yet
	int countEvade;
	boolean gameOver;

	/*
	 * Constructor that initializes the predator and the prey. Also creates all
	 * the Listeners
	 */
	public GamePanel() {

		predator = new Predator();// Predator object

		createPrey();// Creates preys

		/*
		 * ActionListner class for the timer
		 */
		class TimerListener implements ActionListener {

			/*
			 * Method is called when the timer pings it
			 * 
			 * @param e parameter does not need to be sent
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				predator.move();// move predator
				repaint();
				predator.hitWall();// check if predator hit wall
				for (Creature c : prey) {// for every prey
					c.move();// move every prey

					if (c instanceof FastPrey) {// if c is a fast prey
						FastPrey x = (FastPrey) c;
						if (predator.shouldEvade(c)) {// FastPrey should evade

							if (!x.evadeIsRunning) {
								x.evadeStart();// evade
								countEvade++;
							}

							// pass location of the predator
							x.evade(predator.getX() + predator.getHeight() / 2,
									predator.getY() + predator.getHeight() / 2);
						} else if (x.evadeisRunning()) {

							x.evadeStop();// stop evad

						}
					}
					repaint();
				}

				checkCollide();// check if prey has collided with predator
				checkGameOver();// check if game over

			}

			/*
			 * Checks if the preys collided with the predator and removes if it
			 * collided
			 */
			private void checkCollide() {
				for (int i = 0; i < prey.size(); i++) {
					if (predator.collide(prey.get(i))) {
						prey.remove(i);
					}
					repaint();
				}
			}

		}

		TimerListener listener = new TimerListener();
		timer = new Timer(DELAY, listener);// timer that will call
											// actionpermored

		/*
		 * MouseListener class
		 */
		class MousePressListener implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {

			}

			@Override
			public void mouseExited(MouseEvent arg0) {

			}

			/*
			 * Method is called when a mouse button is pressed
			 * 
			 * @param arg0 No need to pass value
			 */
			@Override
			public void mousePressed(MouseEvent arg0) {

				if (!timer.isRunning()) {
					timer.start();// start Timer
					start();// start system clock
				} else if (arg0.getButton() == MouseEvent.BUTTON1) {// if right
					predator.counterClockWise();							// button
					
				} else if (arg0.getButton() == MouseEvent.BUTTON3) {// if left
																	// button
					predator.clockWise();
				}

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {

			}

		}

		mListener = new MousePressListener();
		this.addMouseListener(mListener);

	}

	/*
	 * Checks if game if over when no prey is left and prints time in console if
	 * game over
	 */
	public void checkGameOver() {
		if (prey.size() <= 0) {// if no prey left
			timer.stop();// stop timer
			stop();
			System.out
					.println("Elapsed Time: "
							+ TimeUnit.MILLISECONDS.toMinutes(getDuration())
							+ " minutes and "
							+ (TimeUnit.MILLISECONDS.toSeconds(getDuration()) - (TimeUnit.MILLISECONDS
									.toMinutes(getDuration()) * SECONDS_IN_MINUTE))
							+ " seconds.");
			gameOver = true;
			this.removeMouseListener(mListener);
		}

		repaint();
	}

	/*
	 * Creates the Prey creatures and assigns them a random x and y
	 */
	private void createPrey() {
		Random random = new Random();
		prey = new ArrayList<Creature>();

		for (int i = 0; i < 10; i++) {
			
			//create the prey object with random variables but not to close to the 
			//bouding area
			prey.add(new FastPrey(
					random.nextInt(PacmanFrame.FRAME_WIDTH - 60) + 15, random
							.nextInt(PacmanFrame.FRAME_HEIGHT - 60) + 15 + 15,
					30, 30));
		}
		for (int i = 0; i < 50; i++) {

			prey.add(new SlowPrey(
					random.nextInt(PacmanFrame.FRAME_WIDTH - 60) + 15, random
							.nextInt(PacmanFrame.FRAME_HEIGHT - 60) + 15, 10,
					10));
		}

	}

	/*
	 * Calculates duration of game in milliseconds
	 * 
	 * @return the duration of game in milliseconds
	 */
	public long getDuration() {
		return endTime - startTime;
	}

	/*
	 * Calculate time elapsed of game while game is running
	 * 
	 * @return elapsed time in milliseconds
	 */
	public long getLiveDuration() {
		if (!timer.isRunning()) {
			return 0;
		}
		return System.currentTimeMillis() - startTime;
	}

	/*
	 * Set startTime variable to current time
	 */
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/*
	 * Set endTime variable to current time
	 */
	public void stop() {
		endTime = System.currentTimeMillis();
	}

	/*
	 * Paint component method which draws everything
	 * @param g no need to pass the variale
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);//call super for transparency
		Graphics2D g2 = (Graphics2D) g;
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, PacmanFrame.FRAME_WIDTH, PacmanFrame.FRAME_HEIGHT);//background

		if (timer.isRunning() || count == 0) {//while gamenot over draw prey
			for (Creature p : prey) {
				p.draw(g2);
			}
			predator.draw(g2);
			g2.setColor(Color.white);
			g2.drawString(
					"Elapsed Time: "
							+ TimeUnit.MILLISECONDS
									.toMinutes(getLiveDuration())
							+ " minutes and "
							+ (TimeUnit.MILLISECONDS
									.toSeconds(getLiveDuration()) - (TimeUnit.MILLISECONDS
									.toMinutes(getLiveDuration()) * SECONDS_IN_MINUTE))
							+ " seconds.", 10, 10);//display live time ellapsed
			count++;
		} else {//if game is not running draw static time
			g2.setColor(Color.white);
			g2.drawString(
					"Elapsed Time: "
							+ TimeUnit.MILLISECONDS.toMinutes(getDuration())
							+ " minutes and "
							+ (TimeUnit.MILLISECONDS.toSeconds(getDuration()) - (TimeUnit.MILLISECONDS
									.toMinutes(getDuration()) * SECONDS_IN_MINUTE))
							+ " seconds.", 10, 10);
			
		}
		predator.draw(g2);
	}

}
