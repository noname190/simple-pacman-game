import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;

import javax.swing.Timer;
/*
 * Predator class that contains the shape and design
 * and method to control it
 */
public class Predator extends Creature {

	Ellipse2D.Double predator;
	boolean toggle = false;
	int count = 0;
	int startAngle = 30;//starting angle for the arc
	int endAngle = 300;//angle to add to starting angle for arc

	/*
	 * Predator constructor that creates the width and the predator shape object;
	 */
	public Predator() {
		width = 60;
		height = 60;
		predator = new Ellipse2D.Double(10, 10, width, height);

	}

	/*
	 * Draws the shapes to the panel
	 * @param g2 the Graphics2D variable to draw onto
	 */
	@Override
	public void draw(Graphics2D g2) {

		g2.setColor(Color.yellow);
		
		//Animates the pacman effect
		if (toggle == false) {
			g2.fillArc((int) predator.getX(), (int) predator.getY(),
					(int) predator.getWidth(), (int) predator.getHeight(),
					startAngle, endAngle);
			if (count == 25) {
				toggle = true;
			}
			count++;
		} else {
			g2.fill(predator);
			if (count == 0) {
				toggle = false;
			}
			count--;
		}
		g2.setColor(Color.black);

		
		//Sets location for the pacman eye
		//Not magic numbers, instead i put them there to fine tune
		//the location of the shapes on the face
		if (startAngle == 30) {
			g2.fill(new Ellipse2D.Double(predator.getX() + 30,
					predator.getY() + 11, 7, 7));
		} else if (startAngle == -60) {
			g2.fill(new Ellipse2D.Double(predator.getX() + 40,
					predator.getY() + 28, 7, 7));
		} else if (startAngle == 210) {
			g2.fill(new Ellipse2D.Double(predator.getX() + 25,
					predator.getY() + 11, 7, 7));
		} else {
			g2.fill(new Ellipse2D.Double(predator.getX() + 11,
					predator.getY() + 28, 7, 7));
		}
		// g2.fill(predator);

	}

	/*
	 *Redraws the predator with new x and y values
	 */
	public void move() {
		predator = new Ellipse2D.Double(predator.getX() + xVelocity,
				predator.getY() + yVelocity, width, height);

	}

	/*
	 * Checks if Creature collided 
	 * @param other the Creature to compare to
	 * @return boolean if shapes collide
	 */
	public boolean collide(Creature other) {


		Prey prey = (Prey) other;
		return predator.intersects(prey.getX(), prey.getY(),
				prey.getWidth() / 2, prey.getHeight() / 2);

	}

	/*
	 * Checks if the prey should evade from the predator
	 * @param other the creature to check if it should evade
	 * @return if the predator should evade
	 */
	public boolean shouldEvade(Creature other) {
		Prey prey = (Prey) other;
	
		//Checks the distance
			if (Math.sqrt(Math.pow(predator.getX()+predator.getWidth()/2 - prey.getX()+predator.getWidth()/4, 2)
		+ Math.pow(predator.getY()+predator.getHeight()/2 - prey.getY()+predator.getWidth()/4, 2)) < 60) {
			
			return true;

		} else {
			return false;
		}
	}

	/*
	 * rotates the predator clockwise
	 */
	public void clockWise() {
		if (xVelocity > 0 && yVelocity == 0) {
			yVelocity = xVelocity;
			xVelocity = 0;
			startAngle = -60;

		} else if (xVelocity == 0 && yVelocity > 0) {
			xVelocity = -yVelocity;
			yVelocity = 0;
			startAngle = 210;

			//System.out.println("down");
		} else if (xVelocity < 0 && yVelocity == 0) {
			yVelocity = xVelocity;
			xVelocity = 0;
			startAngle = 120;

			//System.out.println("left");
		} else {
			xVelocity = -yVelocity;
			yVelocity = 0;
			startAngle = 30;

		}
	}

	/*
	 * Rotates the predator counter clockwise
	 */
	public void counterClockWise() {
		if (xVelocity > 0 && yVelocity == 0) {
			yVelocity = -xVelocity;
			xVelocity = 0;
			startAngle = 120;

		} else if (xVelocity == 0 && yVelocity < 0) {
			xVelocity = yVelocity;
			yVelocity = 0;
			startAngle = 210;

		} else if (xVelocity < 0 && yVelocity == 0) {
			yVelocity = -xVelocity;
			xVelocity = 0;
			startAngle = -60;

		} else {
			xVelocity = yVelocity;
			yVelocity = 0;
			startAngle = 30;

		}

	}

	/*
	 * Checks if the predator hit the wall and 
	 * if it did then the predator bounces and goes in 
	 * opposite direction
	 */
	public void hitWall() {
		if (predator.getX() > PacmanFrame.FRAME_WIDTH-getWidth()) {
			xVelocity = -xVelocity;
			startAngle = 210;
		} else if (predator.getX() < 0) {
			xVelocity = -xVelocity;
			startAngle = 30;
		} else if (predator.getY() < 0) {
			yVelocity = -yVelocity;
			startAngle = -60;
		} else if (predator.getY() > PacmanFrame.FRAME_HEIGHT-getHeight()) {
			yVelocity = -yVelocity;
			startAngle = 120;
		}
	}
	
	/*
	 * Returns the x value of the predator shape
	 * return the x value of predator
	 */
	public int getX(){
		return (int)predator.getX();
	}
	

	/*
	 * Returns the y value of the predator shape
	 * return the y value of predator
	 */
	public int getY(){
		return (int)predator.getY();
	}

}
