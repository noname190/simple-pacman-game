import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.JPanel;
/*
 * Creature class that contains boundingBox and other x y and velocity
 */
public class Creature implements MoveableShape {

	protected int x;
	protected int y;
	private int xLeft;
	private int yTop;
	protected int width;
	protected int height;
	private Rectangle boundingBox;
	protected double xVelocity = 2;//staring xVelocity of the creatures
	protected double yVelocity = 0;

	/*
	 * Construter method to create bounding box
	 * @param x x location
	 * @param y y location
	 * @param w width of shape
	 * @param h height of shape
	 */
	public Creature(int x, int y, int w, int h) {
		xLeft = x;
		yTop = y;
		width = w;
		height = h;
		boundingBox = new Rectangle(xLeft, yTop, width, height);

	}

	/*
	 * Constructor without parameters
	 */
	public Creature() {

	}

	/*
	 * Move method
	 */
	@Override
	public void move() {

	}

	/*
	 * Check if shapes collide
	 * @return if shapes collided
	 */
	@Override
	public boolean collide(MoveableShape other) {

		return false;
	}

	/*
	 * Draws the shapes
	 * @param g2 the Graphics2D variables to draw to
	 */
	@Override
	public void draw(Graphics2D g2) {

	}

	/*
	 * Returns x point
	 * @return the x point
	 */
	public int getX() {
		return xLeft;
	}

	/*
	 * Returns y point
	 * @return the y point
	 */
	public int getY() {
		return yTop;
	}

	/*
	 * Returns  width
	 * @return the width of the box
	 */
	public int getWidth() {
		return width;
	}

	/*
	 * Returns x point
	 * @return the height of bounding shape
	 */
	public int getHeight() {
		return height;
	}
	
	/*
	 * Returnsthe boudingBox
	 * @return the boundingBox
	 */
	public Rectangle getBoundingBox(){
		return boundingBox;
	}

}
