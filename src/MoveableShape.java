import java.awt.Graphics2D;
/*
 * Moveable Shape Interface
 */
public interface MoveableShape {
	
	/*
	 * Method to be called when Creature should be moved
	 */
	void move();

	/*
	 * Checks if two MoveableShape objects collided
	 * @param other MoveableShape to compare to
	 */
	boolean collide(MoveableShape other);

	/*
	 * Draw method that has to be implemented
	 * @param g2 Graphics2D type variable that lets you draw
	 */
	void draw(Graphics2D g2);
}
