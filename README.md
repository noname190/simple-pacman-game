# README #

### What is this repository for? ###

* This is for a school assignment
* Goal was to learn how you display shapes on the screen
* It's a pacman style game
* Hardest part was making the prey avoid pacman. You'll notice sometimes it glitches and goes towards pacman

### How do I get set up? ###

Run
>javac PacmanViewer.java

>java PacmanViewer

Controls are Left/Right mouse click